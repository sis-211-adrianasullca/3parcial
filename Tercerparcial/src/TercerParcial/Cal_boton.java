 package TercerParcial;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
/**
 *
 * @author profesor
 */
public class Cal_boton extends JFrame{
    private JLabel lblResultado = new JLabel(" ");
    private JPanel pnlBotones = new JPanel(new GridLayout(4,4));
    private JPanel pnlIgual = new JPanel(new GridLayout(1,1));
    private JButton[] botones = {
        new JButton("1"),  new JButton("2"), new JButton("3"), new JButton("+"),
        new JButton("4"),  new JButton("5"), new JButton("6"), new JButton("-"),  
        new JButton("7"),  new JButton("8"), new JButton("9"), new JButton("*"),
        new JButton("C"),  new JButton("0"), new JButton(","), new JButton("/"),
        new JButton("=")
    };
    private Dimension dmVentana = new Dimension(300,440);
    
    private double resultado = 0;
    private double numero;
    private static final int SUMA = 1;
    private static final int RESTA = 2;
    private static final int MULTIPLICACION = 3;
    private static final int DIVISION = 4;
    private static final int NINGUNO = 0;
    private int operador = Cal_boton.NINGUNO;
    private boolean hayPunto = false;
    private boolean nuevoNumero = true;
    private NumberFormat nf = NumberFormat.getInstance();
    
    public Cal_boton(){
        Dimension dmPantalla = Toolkit.getDefaultToolkit().getScreenSize();
        int a = (dmPantalla.width - dmVentana.width) / 2;
        int b = (dmPantalla.height - dmVentana.height) / 2;
        this.setLocation(a,b);
        this.setSize(dmVentana);
        this.setTitle("Calculadora Basica");
        
        lblResultado.setBackground(Color.white);
        lblResultado.setOpaque(true);
        lblResultado.setFont(new Font("Bell MT", Font.PLAIN,32));
        PulsaRaton pr = new PulsaRaton();
        PulsaTecla pt = new PulsaTecla();
        for (int i=0; i<botones.length-1;i++){
            pnlBotones.add(botones[i]);
            botones[i].addActionListener(pr);
            botones[i].addKeyListener(pt);
        }
        
        pnlIgual.add(botones[botones.length-1]);
        botones[botones.length-1].addActionListener(pr);
        botones[botones.length-1].addKeyListener(pt);
        
        pnlIgual.setPreferredSize(new Dimension(0,50));
        this.add(lblResultado, BorderLayout.NORTH);
        this.add(pnlBotones);
        this.add(pnlIgual,BorderLayout.SOUTH);
                
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
        
        lblResultado.setText(" ");
        lblResultado.setHorizontalAlignment(JLabel.RIGHT);
        
        
        
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        new Cal_boton();
    }
    
    class PulsaRaton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            JButton origen = (JButton) e.getSource();
            String texto = origen.getText();
            accion(texto);
        }

        
    }
    
    class PulsaTecla extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {
            if ( (e.getKeyCode()>=KeyEvent.VK_0 && 
                    e.getKeyCode()<=KeyEvent.VK_9) ||
                    e.getKeyCode() == KeyEvent.VK_COMMA ||
                    e.getKeyCode() == KeyEvent.VK_ADD ||
                    e.getKeyCode() == KeyEvent.VK_MINUS ||
                    e.getKeyCode() == KeyEvent.VK_MULTIPLY ||
                    e.getKeyCode() == KeyEvent.VK_DIVIDE ||
                    e.getKeyCode() == KeyEvent.VK_C ||
                    (e.getKeyCode()>=KeyEvent.VK_NUMPAD0 && 
                    e.getKeyCode()<=KeyEvent.VK_NUMPAD9)){
                accion(""+e.getKeyChar());
            } else if (e.getKeyCode() == KeyEvent.VK_ENTER){
                accion("=");
            }
        }
        
    }
    
    private void accion(String texto) {
            switch (texto) {
                case "+":
                    operar(Cal_boton.SUMA);
                    break;
                case "-":
                    operar(Cal_boton.RESTA);
                    break;
                case "*":
                    operar(Cal_boton.MULTIPLICACION);
                    break;
                case "/":
                    operar(Cal_boton.DIVISION);
                    break;
                case ",":
                    if (!nuevoNumero){
                        if (!hayPunto){
                            String rdo = lblResultado.getText();
                            lblResultado.setText(rdo+",");
                        }
                    } else {
                        lblResultado.setText(" ");
                        nuevoNumero = false;
                    }
                    hayPunto = true;
                    break;
                case "C":
                    lblResultado.setText(" ");
                    nuevoNumero = true;
                    hayPunto = false;
                    break;
                case "=":
                    if (operador != Cal_boton.NINGUNO){
                        String rdo = lblResultado.getText();
                        if (!rdo.isEmpty()){
                            Number n = null;
                            try {
                                n = (Number) nf.parse(rdo);
                                numero = n.doubleValue();
                            } catch (ParseException ex) {
                                numero = 0;
                            }
                            switch (operador) {
                                case Cal_boton.SUMA:
                                    resultado += numero;
                                    break;
                                case Cal_boton.RESTA:
                                    resultado -= numero;
                                    break;
                                case Cal_boton.MULTIPLICACION:
                                    resultado *= numero;
                                    break;
                                case Cal_boton.DIVISION:
                                    resultado /= numero;
                                    break;
                                default:
                                    resultado = numero;
                                    break;
                            }
                            operador = Cal_boton.NINGUNO;
                            lblResultado.setText(nf.format(resultado));
                        }
                    }
                    break;
                default:
                    String rdo = lblResultado.getText();
                    if (nuevoNumero){
                        lblResultado.setText(texto);
                    } else {
                        lblResultado.setText(rdo + texto);
                    }
                    nuevoNumero = false;
                    break;
            }
        }
    
    public void operar(int operacion){
        if (!nuevoNumero){
            String rdo = lblResultado.getText();
            if (!rdo.isEmpty()){
                Number n = null;
                try {
                    n = (Number) nf.parse(rdo);
                    numero = n.doubleValue();
                } catch (ParseException ex) {
                    
                }
                switch (operador) {
                    case Cal_boton.SUMA:
                        resultado += numero;
                        break;
                    case Cal_boton.RESTA:
                        resultado -= numero;
                        break;
                    case Cal_boton.MULTIPLICACION:
                        resultado *= numero;
                        break;
                    case Cal_boton.DIVISION:
                        resultado /= numero;
                        break;
                    default:
                        resultado = numero;
                }
                operador = operacion;
                lblResultado.setText(nf.format(resultado));
                nuevoNumero = true;
                hayPunto = false;
            }
        }
    }
    
}
